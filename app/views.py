from django.http import HttpResponseNotFound
from django.shortcuts import render

# Create your views here.
from sendfile import sendfile
import os

from learn_python.settings import MEDIA_ROOT


def files(request,key):
    key = MEDIA_ROOT + str(key)
    if  os.path.exists(key):
        return sendfile(request,key)
    return HttpResponseNotFound('this is private')
